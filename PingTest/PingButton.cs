﻿using PingTest.Resources;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PingTest
{
    public class PingButton : Button
    {
        public enum PingStatus
        {
            Started,
            Paused
        }

        private PingStatus blinkStatus;
        public PingStatus BlinkStatus
        {
            get
            {
                return blinkStatus;
            }
            set
            {
                if (value == PingStatus.Paused)
                    Text = TextLib.Button_Start;
                else
                    Text = TextLib.Button_Stop;

                blinkStatus = value;
            }
        }
        private Color originalBackColor;

        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);
        }

        public PingButton() : base()
        {
            BlinkStatus = PingStatus.Paused;
            originalBackColor = this.BackColor;
            Blink();
        }

        public void Blink()
        {
            Task task = Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    if (BlinkStatus == PingStatus.Started)
                    {
                        BackColor = BackColor == Color.Red ? Color.Blue : Color.Red;
                    }
                    else
                    {
                        this.BackColor = originalBackColor;
                    }

                    Thread.Sleep(400);
                }
            });
        }

        public void UpdateResource()
        {
            Text = TextLib.Button_Start;
        }
    }
}
