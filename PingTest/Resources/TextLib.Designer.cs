﻿//------------------------------------------------------------------------------
// <auto-generated>
//     이 코드는 도구를 사용하여 생성되었습니다.
//     런타임 버전:4.0.30319.42000
//
//     파일 내용을 변경하면 잘못된 동작이 발생할 수 있으며, 코드를 다시 생성하면
//     이러한 변경 내용이 손실됩니다.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PingTest.Resources {
    using System;
    
    
    /// <summary>
    ///   지역화된 문자열 등을 찾기 위한 강력한 형식의 리소스 클래스입니다.
    /// </summary>
    // 이 클래스는 ResGen 또는 Visual Studio와 같은 도구를 통해 StronglyTypedResourceBuilder
    // 클래스에서 자동으로 생성되었습니다.
    // 멤버를 추가하거나 제거하려면 .ResX 파일을 편집한 다음 /str 옵션을 사용하여 ResGen을
    // 다시 실행하거나 VS 프로젝트를 다시 빌드하십시오.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class TextLib {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal TextLib() {
        }
        
        /// <summary>
        ///   이 클래스에서 사용하는 캐시된 ResourceManager 인스턴스를 반환합니다.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("PingTest.Resources.TextLib", typeof(TextLib).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   이 강력한 형식의 리소스 클래스를 사용하여 모든 리소스 조회에 대한 현재 스레드의 CurrentUICulture
        ///   속성을 재정의합니다.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Cancel과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Button_Cancel {
            get {
                return ResourceManager.GetString("Button_Cancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Save과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Button_Save {
            get {
                return ResourceManager.GetString("Button_Save", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Start과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Button_Start {
            get {
                return ResourceManager.GetString("Button_Start", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Stop과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Button_Stop {
            get {
                return ResourceManager.GetString("Button_Stop", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Exit과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Context_Menu_Exit {
            get {
                return ResourceManager.GetString("Context_Menu_Exit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Open과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Context_Menu_Show {
            get {
                return ResourceManager.GetString("Context_Menu_Show", resourceCulture);
            }
        }
        
        /// <summary>
        ///   IP과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Grid_IP {
            get {
                return ResourceManager.GetString("Grid_IP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Result과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Grid_Result {
            get {
                return ResourceManager.GetString("Grid_Result", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Time과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Grid_Time {
            get {
                return ResourceManager.GetString("Grid_Time", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Language과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Manu_Language {
            get {
                return ResourceManager.GetString("Manu_Language", resourceCulture);
            }
        }
        
        /// <summary>
        ///   About과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Menu_About {
            get {
                return ResourceManager.GetString("Menu_About", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Edit과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Menu_Edit {
            get {
                return ResourceManager.GetString("Menu_Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Exit과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Menu_Exit {
            get {
                return ResourceManager.GetString("Menu_Exit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   File과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Menu_File {
            get {
                return ResourceManager.GetString("Menu_File", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Go Tray과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Menu_GoTray {
            get {
                return ResourceManager.GetString("Menu_GoTray", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Help과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Menu_Help {
            get {
                return ResourceManager.GetString("Menu_Help", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Manual과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Menu_Manual {
            get {
                return ResourceManager.GetString("Menu_Manual", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Option과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Menu_Option {
            get {
                return ResourceManager.GetString("Menu_Option", resourceCulture);
            }
        }
        
        /// <summary>
        ///   [Yes : Exit ] [No : Go Tray]과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Message_Exit_Content {
            get {
                return ResourceManager.GetString("Message_Exit_Content", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Exit과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Message_Exit_Title {
            get {
                return ResourceManager.GetString("Message_Exit_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Interval(sec)과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Option_Inverval {
            get {
                return ResourceManager.GetString("Option_Inverval", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Log Folder과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Option_LogFolder {
            get {
                return ResourceManager.GetString("Option_LogFolder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Save Fail Log Only과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Option_SaveFailOnly {
            get {
                return ResourceManager.GetString("Option_SaveFailOnly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Option과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Option_Title {
            get {
                return ResourceManager.GetString("Option_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ready과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Status_Ready {
            get {
                return ResourceManager.GetString("Status_Ready", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Start Time과(와) 유사한 지역화된 문자열을 찾습니다.
        /// </summary>
        internal static string Status_Time {
            get {
                return ResourceManager.GetString("Status_Time", resourceCulture);
            }
        }
    }
}
