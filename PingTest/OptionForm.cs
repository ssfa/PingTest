﻿using PingTest.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static PingTest.Options;

namespace PingTest
{
    public partial class OptionForm : Form
    {
        public OptionForm()
        {
            InitializeComponent();
        }

        private void OptionForm_Load(object sender, EventArgs e)
        {
            tbLogFolder.Text = Options.Instance.Path;
            tbInterval.Text = Options.Instance.Interval.ToString();
            cbIsFailOnly.Checked = Options.Instance.FailOnly;

            if (Options.Instance.Culture.Equals("English"))
            {
                radioEngilsh.Checked = true;
            }
            else
            {
                radioKorean.Checked = true;
            }

            UpdateResource(Options.Instance.Culture);

            foreach (var ip in Options.Instance.IPList)
            {
                gridIPList.Rows.Add(ip.Alias, ip.IPAddress);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Options.Instance.Path = tbLogFolder.Text;
            Options.Instance.Interval = int.Parse(tbInterval.Text);
            Options.Instance.FailOnly = cbIsFailOnly.Checked;

            Options.Instance.IPList.Clear();
            foreach (DataGridViewRow row in gridIPList.Rows)
            {
                if (row.Cells[0].Value != null)
                {
                    string alias = row.Cells[0].Value.ToStringEx();
                    string ip = row.Cells[1].Value.ToStringEx();

                    if (ValidateIPv4(ip))
                    {
                        Options.Instance.IPList.Add(new Options.IP() { IPAddress = ip, Alias = alias });
                    }
                    else
                    {
                        MessageBox.Show("Check IP Address.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }

            Options.Instance.Culture = radioKorean.Checked ? "Korean" : "English";

            Options.Save();
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Options.Refresh();
            Close();
        }

        private void tbInterval_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(tbInterval.Text, "  ^ [0-9]"))
                tbInterval.Text = "";
        }

        private void tbInterval_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }

        private bool ValidateIPv4(string ipString)
        {
            if (String.IsNullOrWhiteSpace(ipString))
            {
                return false;
            }

            string[] splitValues = ipString.Split('.');
            if (splitValues.Length != 4)
            {
                return false;
            }

            byte tempForParsing;

            return splitValues.All(r => byte.TryParse(r, out tempForParsing));
        }

        private void btnSelectFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folder = new FolderBrowserDialog();

            folder.SelectedPath = tbLogFolder.Text;

            if (folder.ShowDialog() == DialogResult.OK)
                tbLogFolder.Text = folder.SelectedPath;
        }

        private void UpdateResource(string culture)
        {
            if (culture == "English")
            {
                TextLib.Culture = new CultureInfo("en");
            }
            else
            {
                TextLib.Culture = new CultureInfo("ko-KR");
            }

            lbInterval.Text = TextLib.Option_Inverval;
            lbLogFolder.Text = TextLib.Option_LogFolder;
            lbLanguage.Text = TextLib.Manu_Language;

            cbIsFailOnly.Text = TextLib.Option_SaveFailOnly;
            IP.HeaderText = TextLib.Grid_IP;

            btnSave.Text = TextLib.Button_Save;
            btnCancel.Text = TextLib.Button_Cancel;
        }
    }
}
