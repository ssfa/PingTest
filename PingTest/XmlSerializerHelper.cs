﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace PingTest
{
    public static class XmlSerializerHelper
    {
        public static T Load<T>(string fileName, T defaultValue = default(T))
        {
            return Load(fileName, null, defaultValue);
        }

        public static T Load<T>(string fileName, string rootName, T defaultValue = default(T))
        {
            try
            {
                XmlSerializer xmlSerializer = CreateXmlSerializer<T>(rootName);

                using (StreamReader reader = new StreamReader(fileName))
                {
                    return (T)xmlSerializer.Deserialize(reader);
                }
            }
            catch
            {
                return defaultValue;
            }
        }

        public static T LoadFromString<T>(string serializedString, T defaultValue = default(T))
        {
            return LoadFromString(serializedString, null, defaultValue);
        }

        public static T LoadFromString<T>(string serializedString, string rootName, T defaultValue = default(T))
        {
            try
            {
                XmlSerializer xmlSerializer = CreateXmlSerializer<T>(rootName);

                using (StringReader reader = new StringReader(serializedString))
                {
                    return (T)xmlSerializer.Deserialize(reader);
                }
            }
            catch
            {
                return defaultValue;
            }
        }

        public static void Save<T>(T value, string fileName, string rootName = null)
        {
            var emptyNamespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            XmlWriterSettings setting = new XmlWriterSettings()
            {
                Indent = true,
                OmitXmlDeclaration = true
            };

            XmlSerializer xmlSerializer = CreateXmlSerializer<T>(rootName);

            using (XmlWriter writer = XmlWriter.Create(fileName, setting))
            {
                xmlSerializer.Serialize(writer, value, emptyNamespaces);
            }
        }

        public static string ToString<T>(T value, string rootName = null)
        {
            var emptyNamespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            XmlWriterSettings setting = new XmlWriterSettings()
            {
                Indent = true,
                OmitXmlDeclaration = true
            };

            XmlSerializer xmlSerializer = CreateXmlSerializer<T>(rootName);

            using (StringWriter sw = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sw, setting))
                {
                    xmlSerializer.Serialize(writer, value, emptyNamespaces);

                    return sw.ToString();
                }
            }
        }

        private static XmlSerializer CreateXmlSerializer<T>(string rootName = null)
        {
            if (string.IsNullOrEmpty(rootName))
                return new XmlSerializer(typeof(T));
            else
                return new XmlSerializer(typeof(T), new XmlRootAttribute(rootName));
        }
    }
}
