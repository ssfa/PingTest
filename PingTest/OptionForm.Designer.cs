﻿namespace PingTest
{
    partial class OptionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionForm));
            this.lbInterval = new System.Windows.Forms.Label();
            this.lbLogFolder = new System.Windows.Forms.Label();
            this.tbInterval = new System.Windows.Forms.TextBox();
            this.tbLogFolder = new System.Windows.Forms.TextBox();
            this.btnSelectFolder = new System.Windows.Forms.Button();
            this.cbIsFailOnly = new System.Windows.Forms.CheckBox();
            this.gridIPList = new System.Windows.Forms.DataGridView();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radioEngilsh = new System.Windows.Forms.RadioButton();
            this.radioKorean = new System.Windows.Forms.RadioButton();
            this.lbLanguage = new System.Windows.Forms.Label();
            this.IP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Alias = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridIPList)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbInterval
            // 
            this.lbInterval.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbInterval.AutoSize = true;
            this.lbInterval.Location = new System.Drawing.Point(3, 39);
            this.lbInterval.Name = "lbInterval";
            this.lbInterval.Size = new System.Drawing.Size(81, 12);
            this.lbInterval.TabIndex = 8;
            this.lbInterval.Text = "Interval (Sec)";
            // 
            // lbLogFolder
            // 
            this.lbLogFolder.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbLogFolder.AutoSize = true;
            this.lbLogFolder.Location = new System.Drawing.Point(3, 9);
            this.lbLogFolder.Name = "lbLogFolder";
            this.lbLogFolder.Size = new System.Drawing.Size(65, 12);
            this.lbLogFolder.TabIndex = 9;
            this.lbLogFolder.Text = "Log Folder";
            // 
            // tbInterval
            // 
            this.tbInterval.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tbInterval.Location = new System.Drawing.Point(107, 34);
            this.tbInterval.Name = "tbInterval";
            this.tbInterval.Size = new System.Drawing.Size(41, 21);
            this.tbInterval.TabIndex = 6;
            this.tbInterval.TextChanged += new System.EventHandler(this.tbInterval_TextChanged);
            this.tbInterval.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbInterval_KeyPress);
            // 
            // tbLogFolder
            // 
            this.tbLogFolder.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel1.SetColumnSpan(this.tbLogFolder, 3);
            this.tbLogFolder.Location = new System.Drawing.Point(107, 4);
            this.tbLogFolder.Name = "tbLogFolder";
            this.tbLogFolder.ReadOnly = true;
            this.tbLogFolder.Size = new System.Drawing.Size(281, 21);
            this.tbLogFolder.TabIndex = 7;
            // 
            // btnSelectFolder
            // 
            this.btnSelectFolder.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSelectFolder.Location = new System.Drawing.Point(394, 3);
            this.btnSelectFolder.Name = "btnSelectFolder";
            this.btnSelectFolder.Size = new System.Drawing.Size(37, 23);
            this.btnSelectFolder.TabIndex = 5;
            this.btnSelectFolder.Text = "...";
            this.btnSelectFolder.UseVisualStyleBackColor = true;
            this.btnSelectFolder.Click += new System.EventHandler(this.btnSelectFolder_Click);
            // 
            // cbIsFailOnly
            // 
            this.cbIsFailOnly.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbIsFailOnly.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.cbIsFailOnly, 3);
            this.cbIsFailOnly.Location = new System.Drawing.Point(194, 37);
            this.cbIsFailOnly.Name = "cbIsFailOnly";
            this.cbIsFailOnly.Size = new System.Drawing.Size(106, 16);
            this.cbIsFailOnly.TabIndex = 10;
            this.cbIsFailOnly.Text = "Save Fail Only";
            this.cbIsFailOnly.UseVisualStyleBackColor = true;
            // 
            // gridIPList
            // 
            this.gridIPList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridIPList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IP,
            this.Alias});
            this.tableLayoutPanel1.SetColumnSpan(this.gridIPList, 5);
            this.gridIPList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridIPList.Location = new System.Drawing.Point(3, 103);
            this.gridIPList.MultiSelect = false;
            this.gridIPList.Name = "gridIPList";
            this.gridIPList.RowTemplate.Height = 23;
            this.gridIPList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridIPList.Size = new System.Drawing.Size(488, 294);
            this.gridIPList.TabIndex = 11;
            // 
            // btnCancel
            // 
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancel.Location = new System.Drawing.Point(394, 403);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(97, 41);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSave.Location = new System.Drawing.Point(294, 403);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(94, 41);
            this.btnSave.TabIndex = 13;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.27136F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.72864F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 102F));
            this.tableLayoutPanel1.Controls.Add(this.tbLogFolder, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnSave, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.tbInterval, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnCancel, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.gridIPList, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnSelectFolder, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbIsFailOnly, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbLogFolder, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbInterval, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radioEngilsh, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.radioKorean, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbLanguage, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(494, 447);
            this.tableLayoutPanel1.TabIndex = 14;
            // 
            // radioEngilsh
            // 
            this.radioEngilsh.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioEngilsh.AutoSize = true;
            this.radioEngilsh.Location = new System.Drawing.Point(107, 67);
            this.radioEngilsh.Name = "radioEngilsh";
            this.radioEngilsh.Size = new System.Drawing.Size(65, 16);
            this.radioEngilsh.TabIndex = 12;
            this.radioEngilsh.TabStop = true;
            this.radioEngilsh.Text = "English";
            this.radioEngilsh.UseVisualStyleBackColor = true;
            // 
            // radioKorean
            // 
            this.radioKorean.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioKorean.AutoSize = true;
            this.radioKorean.Location = new System.Drawing.Point(194, 67);
            this.radioKorean.Name = "radioKorean";
            this.radioKorean.Size = new System.Drawing.Size(59, 16);
            this.radioKorean.TabIndex = 12;
            this.radioKorean.TabStop = true;
            this.radioKorean.Text = "한국어";
            this.radioKorean.UseVisualStyleBackColor = true;
            // 
            // lbLanguage
            // 
            this.lbLanguage.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbLanguage.AutoSize = true;
            this.lbLanguage.Location = new System.Drawing.Point(3, 69);
            this.lbLanguage.Name = "lbLanguage";
            this.lbLanguage.Size = new System.Drawing.Size(61, 12);
            this.lbLanguage.TabIndex = 8;
            this.lbLanguage.Text = "Language";
            // 
            // IP
            // 
            this.IP.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.IP.HeaderText = "IP";
            this.IP.Name = "IP";
            // 
            // Alias
            // 
            this.Alias.HeaderText = "Alias";
            this.Alias.Name = "Alias";
            this.Alias.Width = 200;
            // 
            // OptionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 471);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OptionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Option";
            this.Load += new System.EventHandler(this.OptionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridIPList)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbInterval;
        private System.Windows.Forms.Label lbLogFolder;
        private System.Windows.Forms.TextBox tbInterval;
        private System.Windows.Forms.TextBox tbLogFolder;
        private System.Windows.Forms.Button btnSelectFolder;
        private System.Windows.Forms.CheckBox cbIsFailOnly;
        private System.Windows.Forms.DataGridView gridIPList;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.RadioButton radioEngilsh;
        private System.Windows.Forms.RadioButton radioKorean;
        private System.Windows.Forms.Label lbLanguage;
        private System.Windows.Forms.DataGridViewTextBoxColumn IP;
        private System.Windows.Forms.DataGridViewTextBoxColumn Alias;
    }
}