﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PingTest
{
    public class LogManager
    {
        public static void Write(string path, string message)
        {
            FileStream stream = null;
            FileInfo logFileInfo = new FileInfo(path);
            DirectoryInfo logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            
            if (!logDirInfo.Exists)
                logDirInfo.Create();

            if (!logFileInfo.Exists)
                stream = logFileInfo.Create();
            else
                stream = new FileStream(path, FileMode.Append);

            using (StreamWriter log = new StreamWriter(stream))
            {
                log.WriteLine(string.Format("[{0}]___{1}", DateTime.Now.ToString(), message));
            }

            stream.Close();
        }
    }
}
