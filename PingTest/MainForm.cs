﻿using PingTest.Resources;
using System;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PingTest
{
    public partial class MainForm : Form
    {
        private bool IsStart;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UpdateResource(Options.Instance.Culture);
            RefreshGrid();
            pingTimer.Interval = Options.Instance.Interval * 1000;
        }

        private void RefreshGrid()
        {
            gridResult.Rows.Clear();

            Options.Instance.IPList.ForEach(ip =>
            {
                gridResult.Rows.Add("Ready", ip.Alias, ip.IPAddress);
            });
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void optionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OptionForm optionForm = new OptionForm();
            optionForm.ShowDialog();

            UpdateResource(Options.Instance.Culture);
            pingTimer.Interval = Options.Instance.Interval * 1000;
            RefreshGrid();
        }

        private async void PingAll()
        {
            var pingTasks = Options.Instance.IPList.Select(ip =>
            {
                return new Ping().SendTaskAsync(ip.IPAddress);
            });

            var replies = await Task.WhenAll(pingTasks);
            DateTime moment = DateTime.Now;

            foreach (var r in replies)
            {
                string path = string.Format("{0}\\{1}_{2}.txt", Options.Instance.Path, moment.ToString("yyyyMMdd"), r.Address);

                if (Options.Instance.FailOnly)
                {
                    if (r.Reply.Status != IPStatus.Success)
                        LogManager.Write(path, r.Reply.Status.ToString());
                }
                else
                {
                    LogManager.Write(path, r.Reply.Status.ToString());
                }

                foreach (DataGridViewRow row in gridResult.Rows)
                {
                    if (row.Cells[2].Value != null)
                    {
                        if (string.Equals(row.Cells[2].Value.ToString(), r.Address, StringComparison.OrdinalIgnoreCase))
                        {
                            row.Cells[0].Value = moment.ToString("hh:mm:ss");
                            row.Cells[3].Value = r.Reply.Status.ToString();
                        }
                    }
                }
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            IsStart = !IsStart;

            if (IsStart)
            {
                btnStart.BlinkStatus = PingButton.PingStatus.Started;
                pingTimer.Start();
                toolStripStatusLabelStart.Text = DateTime.Now.ToString("yyyyMMdd_hh:mm:ss");
            }
            else
            {
                btnStart.BlinkStatus = PingButton.PingStatus.Paused;
                toolStripStatusLabelStart.Text = TextLib.Status_Ready;
                pingTimer.Stop();
            }
        }

        private void pingTimer_Tick(object sender, EventArgs e)
        {
            pingTimer.Stop();
            PingAll();
            pingTimer.Start();
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToggleTrayMode(false);
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void goTrayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToggleTrayMode(true);
        }

        private void ToggleTrayMode(bool isTray)
        {
            if (isTray)
            {
                WindowState = FormWindowState.Minimized;
                ShowInTaskbar = false;
                Visible = false;
            }
            else
            {
                WindowState = FormWindowState.Normal;
                ShowInTaskbar = true;
                Visible = true;
            }
        }

        private void notifyIconTray_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ToggleTrayMode(false);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show(TextLib.Message_Exit_Content, TextLib.Message_Exit_Title, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            if (result == DialogResult.No)
            {
                ToggleTrayMode(true);
                e.Cancel = true;
            }
            else if (result == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
        }

        private void UpdateResource(string culture)
        {
            if(culture == "English")
            {
                TextLib.Culture = new System.Globalization.CultureInfo("en");
            }
            else
            {
                TextLib.Culture = new System.Globalization.CultureInfo("ko-KR");
            }

            fileToolStripMenuItem.Text = TextLib.Menu_File;
            editToolStripMenuItem.Text = TextLib.Menu_Edit;
            helpToolStripMenuItem.Text = TextLib.Menu_Help;
            aboutToolStripMenuItem.Text = TextLib.Menu_About;
            goTrayToolStripMenuItem.Text = TextLib.Menu_GoTray;
            exitToolStripMenuItem.Text = TextLib.Menu_Exit;
            optionToolStripMenuItem.Text = TextLib.Menu_Option;
            manualToolStripMenuItem.Text = TextLib.Menu_Manual;

            Time.HeaderText = TextLib.Grid_Time;
            IP.HeaderText = TextLib.Grid_IP;
            Result.HeaderText = TextLib.Grid_Result;

            toolStripStatusLabelStart.Text = TextLib.Status_Ready;
            toolStripStatusLabel.Text = TextLib.Status_Time;

            showContextToolStripMenuItem.Text = TextLib.Context_Menu_Show;
            exitContextToolStripMenuItem.Text = TextLib.Context_Menu_Exit;

            btnStart.UpdateResource();
        }
    }
}
