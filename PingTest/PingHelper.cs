﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace PingTest
{
    public static class PingHelper
    {
        public static Task<PingResult> SendTaskAsync(this Ping ping, string address)
        {
            var tcs = new TaskCompletionSource<PingResult>();
            PingCompletedEventHandler response = null;
            response = (s, e) =>
            {
                ping.PingCompleted -= response;
                tcs.SetResult(new PingResult() { Address = address, Reply = e.Reply });
            };
            ping.PingCompleted += response;
            ping.SendAsync(address,1000, address);
            return tcs.Task;
        }

        public class PingResult
        {
            public string Address { set; get; }
            public PingReply Reply { set; get; }
        }

    }
}
