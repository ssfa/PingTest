﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PingTest
{
    public class Options : OptionBase<Options>
    {
        public Options()
        {
            LoadDefault();
        }

        private void LoadDefault()
        {
            Interval = 2;
            Path = Environment.CurrentDirectory + "\\" + "Log";
            FailOnly = false;
            IPList = new List<IP>();
            Culture = "English";
        }

        public struct IP
        {
            public string IPAddress { get; set; }
            public string Alias { get; set; }
        }

        public int Interval { get; set; }
        public string Path { get; set; }
        public bool FailOnly { get; set; }
        public List<IP> IPList { get; set; }
        public string Culture { get; set; }
    }
}
