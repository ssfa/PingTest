﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PingTest
{
    public class OptionBase<T> where T : class, new()
    {
        private static string OptionFileName = "option.opt";

        private static T instance;
        public static T Instance
        {
            get
            {
                if (instance == null)
                    instance = XmlSerializerHelper.Load<T>(OptionFileName, new T());
                return instance;
            }
        }

        public static void Save()
        {
            XmlSerializerHelper.Save(Instance, OptionFileName);
        }

        public static void Refresh()
        {
            instance = default(T);
        }
    }
}
