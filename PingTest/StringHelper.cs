﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PingTest
{
    public static class StringHelper
    {
        public static string ToStringEx(this object value)
        {
            if (value == null)
                return string.Empty;

            return value.ToString();
        }
    }
}
